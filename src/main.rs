use rand::Rng;
use rodio::Source;
use std::fs::File;
use std::io::BufReader;
use std::{thread, time};

const MULT: u64 = 1;

fn main() {
    let mut rng = rand::thread_rng();
    let device = rodio::default_output_device().unwrap();

    let file_name_list = ["1.mp3", "2.mp3", "3.mp3"];

    loop {
        let file = File::open(file_name_list[rng.gen_range(0, file_name_list.len())]).unwrap();
        println!(".");

        let source = rodio::Decoder::new(BufReader::new(file)).unwrap();
        rodio::play_raw(&device, source.convert_samples());

        let wait_time = time::Duration::from_secs(rng.gen_range(3 * MULT, 5 * MULT));
        thread::sleep(wait_time);
    }
}
